using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class TeaSet_InteractionController : MonoBehaviour
{
    private Animator animator;
    private XRGrabInteractable grabInteractable;
    void Start()
    {
        animator = GetComponent<Animator>();
    }



    public void PlayAnimation_PourTea()
    {
        animator.SetBool("IsPouring", true);
        StartCoroutine(WaitReset());
    }

    IEnumerator WaitReset()
    {
           yield return new WaitForSeconds(1.0f);
        animator.SetBool("IsPouring", false);
    }

}
