using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SayHello : MonoBehaviour
{
    public void SayHelloo() { Debug.Log("<color=green>Hello</color>"); }
    public void SaySomethingElse(string somethingElse)
    {
        Debug.Log($"<color=green>{somethingElse}</color>");
    }
}
