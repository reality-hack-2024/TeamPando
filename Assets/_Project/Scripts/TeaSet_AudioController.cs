using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SFXClip
{
    public AudioClip clip;
    public float volume = 1;
}
public class TeaSet_AudioController : MonoBehaviour
{
    public AudioSource audioSource;
    [SerializeField] private SFXClip pickUpClip;
    [SerializeField] private SFXClip putDownClip;
    [SerializeField] private SFXClip pourTeaClip;
    [SerializeField] private SFXClip chimeClip;

    public void PlayAudio_PickUp()
    {
        audioSource.PlayOneShot(pickUpClip.clip, pickUpClip.volume );
    }
    public void PlayAudio_PutDown()
    {
        audioSource.PlayOneShot(putDownClip.clip, putDownClip.volume);
    }
    public void PlayAudio_PourTea()
    {
        audioSource.PlayOneShot(pourTeaClip.clip, pourTeaClip.volume);
    }
    public void PlayAudio_Chime()
    {
        audioSource.PlayOneShot(chimeClip.clip, chimeClip.volume);
    }

}
